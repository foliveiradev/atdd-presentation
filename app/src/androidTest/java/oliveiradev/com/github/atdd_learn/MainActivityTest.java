package oliveiradev.com.github.atdd_learn;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollTo;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;

/**
 * Created by felipe on 18/11/16.
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule =
            new ActivityTestRule(MainActivity.class);

    @Test
    public void shouldBeSuccessOnTaskCreation() throws Exception {
        String newTask = "Fazer coisa tal";

        onView(withId(R.id.description)).perform(typeText(newTask), closeSoftKeyboard());

        onView(withId(R.id.save))
                .perform(click());

        onView(withId(R.id.tasks))
                .perform(scrollTo(hasDescendant(withText(newTask))));

        onView(withText(newTask))
                .check(matches(isDisplayed()));
    }


    @Test
    public void shouldBeInvalidTaskContext() throws Exception {
        onView(withId(R.id.description)).perform(typeText(""), closeSoftKeyboard());

        onView(withId(R.id.save))
                .perform(click());

        onView(withText("Task inválida"))
                .inRoot(withDecorView(not(is(mainActivityTestRule.getActivity().getWindow().getDecorView()))))
                .check(matches(isDisplayed()));
    }
}
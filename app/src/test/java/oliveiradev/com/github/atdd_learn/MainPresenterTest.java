package oliveiradev.com.github.atdd_learn;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import oliveiradev.com.github.atdd_learn.domain.Task;

import static org.mockito.Mockito.verify;

/**
 * Created by felipe on 17/11/16.
 */
public class MainPresenterTest {

    @Mock
    private MainContract.View view;

    private MainPresenter presenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        presenter = new MainPresenter(view);
    }

    @Test
    public void createTask_addOnAdapter() {
        Task task = new Task("Fazer coisa tal");

        //when
        presenter.createTask(task);

        //then
        verify(view).addOnAdapter(task);
    }

    @Test
    public void finishTask_removeTaskOfAdapter() {
        Task task = new Task("Fazer coisa tal");

        //when
        presenter.finishTask(task);

        //then
        verify(view).removeOfAdapter(task);
    }

    @Test
    public void createInvalidTask_showErrorInvalidTask() {
        Task task = new Task("");

        //when
        presenter.createTask(task);

        //then
        verify(view).showErrorInvalidTask();
    }
}
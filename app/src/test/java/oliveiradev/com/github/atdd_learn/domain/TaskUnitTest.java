package oliveiradev.com.github.atdd_learn.domain;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;


/**
 * Created by felipe on 17/11/16.
 */
public class TaskUnitTest {

    private Task task;

    @Before
    public void setUp() {
        task = new Task("Test");
    }

    @Test
    public void testIfTaskIsValid() throws Exception {
        final boolean expected = true;
        assertThat("Invalid task", task.isValidTask(), is(expected));
    }
}
package oliveiradev.com.github.atdd_learn.helper;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by felipe on 16/11/16.
 */
public class FormatHelperTest {

    @Test
    public void testIfFormatDateToStringReturnCorrectly() throws Exception {
        final String expected = "10/10/2010";
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 10);
        calendar.set(Calendar.MONTH, 9);
        calendar.set(Calendar.YEAR, 2010);
        final Date result = calendar.getTime();


        assertThat("Date not formatted correctly", FormatHelper.dateToString(result), is(expected));
    }
}
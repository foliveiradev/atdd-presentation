package oliveiradev.com.github.atdd_learn;

import oliveiradev.com.github.atdd_learn.domain.Task;

/**
 * Created by felipe on 17/11/16.
 */
public class MainPresenter implements MainContract.Presenter {

    private final MainContract.View view;

    public MainPresenter(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void createTask(Task task) {
        if (task.isValidTask())
            view.addOnAdapter(task);
        else
            view.showErrorInvalidTask();
    }

    @Override
    public void finishTask(Task task) {
        view.removeOfAdapter(task);
    }
}

package oliveiradev.com.github.atdd_learn;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.LinkedList;

import oliveiradev.com.github.atdd_learn.databinding.ItemTaskBinding;
import oliveiradev.com.github.atdd_learn.domain.Task;
import oliveiradev.com.github.atdd_learn.helper.FormatHelper;

/**
 * Created by felipe on 17/11/16.
 */
public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private LinkedList<Task> tasks;
    private MainContract.Presenter presenter;

    public MainAdapter(MainContract.Presenter presenter) {
        this.tasks = new LinkedList<>();
        this.presenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(/*ItemTaskBinding.inflate(inflater)*/inflater.inflate(R.layout.item_task, parent, false), presenter);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(tasks.get(position));
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }

    public void add(Task task) {
        tasks.add(task);
        notifyItemInserted(itemIndex(task));
    }

    public void remove(Task task) {
        tasks.remove(task);
        notifyItemRemoved(itemIndex(task));
    }

    private int itemIndex(Task task) {
        return tasks.indexOf(task);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        //private final ItemTaskBinding binding;
        private MainContract.Presenter presenter;
        private CheckBox ready;
        private TextView description;
        private TextView createdAt;

        public ViewHolder(/*ItemTaskBinding binding*/View itemView, MainContract.Presenter presenter) {
            super(itemView);
            this.presenter = presenter;
            description = (TextView) itemView.findViewById(R.id.description);
            createdAt = (TextView) itemView.findViewById(R.id.created_at);
            ready = (CheckBox) itemView.findViewById(R.id.ready);
        }

        public void bind(final Task task) {
            description.setText(task.getDescription());
            createdAt.setText(FormatHelper.dateToString(task.getCreatedAt()));

            ready.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b)
                        presenter.finishTask(task);
                }
            });
        }
    }
}

package oliveiradev.com.github.atdd_learn.helper;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by felipe on 16/11/16.
 */
public final class FormatHelper {

    private static final String PATTERN = "dd/MM/yyyy";

    public static String dateToString(Date date){
        final SimpleDateFormat formatter = new SimpleDateFormat(PATTERN);
        return formatter.format(date);
    }
}

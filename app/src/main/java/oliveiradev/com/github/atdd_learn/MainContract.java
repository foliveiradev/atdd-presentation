package oliveiradev.com.github.atdd_learn;

import oliveiradev.com.github.atdd_learn.domain.Task;

/**
 * Created by felipe on 17/11/16.
 */
public interface MainContract {

    interface View {
        void addOnAdapter(Task task);
        void removeOfAdapter(Task task);
        void showErrorInvalidTask();
    }
    interface Presenter {
        void createTask(Task task);
        void finishTask(Task task);
    }
}

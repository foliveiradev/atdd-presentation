package oliveiradev.com.github.atdd_learn;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import oliveiradev.com.github.atdd_learn.databinding.ActivityMainBinding;
import oliveiradev.com.github.atdd_learn.domain.Task;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    //private ActivityMainBinding binding;
    private MainContract.Presenter presenter;
    private MainAdapter adapter;
    private RecyclerView tasks;
    private Button save;
    private TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*binding = DataBindingUtil.*/
        setContentView(R.layout.activity_main);
        presenter = new MainPresenter(this);
        adapter = new MainAdapter(presenter);

        init();
    }

    private void init() {
        tasks = (RecyclerView) findViewById(R.id.tasks);
        save = (Button) findViewById(R.id.save);
        description = (TextView) findViewById(R.id.description);

        tasks.setHasFixedSize(true);
        tasks.setLayoutManager(new LinearLayoutManager(this));
        tasks.setAdapter(adapter);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.createTask(new Task(description.getText().toString()));
            }
        });
    }

    @Override
    public void addOnAdapter(Task task) {
        description.setText("");
        adapter.add(task);
    }

    @Override
    public void removeOfAdapter(Task task) {
        adapter.remove(task);
    }

    @Override
    public void showErrorInvalidTask() {
        Toast.makeText(this, "Task inválida", Toast.LENGTH_LONG).show();
    }
}

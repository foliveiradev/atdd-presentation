package oliveiradev.com.github.atdd_learn.domain;

import java.util.Date;

/**
 * Created by felipe on 16/11/16.
 */
public class Task {

    private String description;
    private boolean pending;
    private Date createdAt;

    public Task(String description) {
        this.description = description;
        this.pending = true;
        this.createdAt = new Date();
    }

    public String getDescription() {
        return description;
    }

    public Boolean getPending() {
        return pending;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public boolean isValidTask() {
        if (description == null || description.isEmpty())
            return false;

        return true;
    }
}
